import {LitElement, html} from 'lit-element';

class FichaPersona extends LitElement {
    static get properties(){
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        };
    }

    constructor () {
        super();

        this.name = "Prueba Nombre";
        this.yearsInCompany = 12;
        this.photo = {
            src: "./img/persona.jpg",
            alt: "Foto Persona"
        }
        this.updatePersonInfo();
    }
    render(){
        return html `
            <div>Ficha Persona</div>
            <div>
                <label for="fname">Nombre Completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br/>

                <label for="yearsInCompany">Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br/>

                <label for="personInfo">Puesto</label>
                <input type="text" name="personInfo" value="${this.personInfo}" disabled></input>
                <br/>

                <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}"></img>

            </div>
        `;
    }

    updated (changedProperties) {
        if  (changedProperties.has("name")){
            console.log("Propiedad name cambio valor de "+ changedProperties.get("name") + " a " + this.name);
        }
        if  (changedProperties.has("yearsInCompany")){
            console.log("Propiedad yearsInCompany cambio valor de "+ changedProperties.get("yearsInCompany") + " a " + this.yearsInCompany);
            this.updatePersonInfo();
        }
        if  (changedProperties.has("personInfo")){
            console.log("Propiedad personInfo cambio valor de "+ changedProperties.get("personInfo") + " a " + this.personInfo);
        }


    }

    updateName(e){
        console.log("updateName");
        this.name = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo() {
        console.log("updatePersonInfo");
        console.log("yearsInCompany vale " + this.yearsInCompany);
        
        if (this.yearsInCompany >= 7 ) {
            this.personInfo = "Lead";
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "Senior";
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "Team";
        } else {
            this.personInfo = "Junior";
        }
    }
}

customElements.define('ficha-persona',FichaPersona)