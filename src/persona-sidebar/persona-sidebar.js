import {LitElement, html} from 'lit-element';

class PersonaSidebar extends LitElement {
    
    static get properties() {
        return {
            peopleStats: {type: Object}
        };
    }

    constructor () {
        super();
        this.peopleStats = {};
    }

    render(){
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <aside>
                <section>
                    <div>
                        Hay <span class="badge badge-pill badge-primary">${this.peopleStats.numberOfPeople}</span> personas
                    </div>
                    <div class="mt-5">
                        <button @click="${this.newPerson}" class="w-100 btn bg-success" style="font-size: 50px"><strong>+</strong></button>
                    </div>
                </section>
            </aside>
            
        `;
    }

    newPerson(e){
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear un nuevo registro de persona");
        this.dispatchEvent(
            new CustomEvent("new-person",{})
        );
    }
}

customElements.define('persona-sidebar',PersonaSidebar)