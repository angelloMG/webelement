import {LitElement, html} from 'lit-element';

class EmisorEvento extends LitElement {

    static get properties() {
        return {

        };
    }

    constructor() {
        super();
    }

    //@click se encarga de enviar el evento al realizar un click
    render(){
        return html `
            <h3>Emisor evento</h3>
            <button @click="${this.sendEvent}">No Pulsar</button> 
        `;
    }

    // e viene a ser el evento y detail son los datos que viajan con él
    sendEvent (e) {
        console.log("Se ha pulsado el botón");
        console.log(e);

        this.dispatchEvent (
            new CustomEvent (
                "test-event",
                {
                    detail: {
                        "course" :"TechU",
                        "year": 2020
                    }
                }
            )
        )
    }
}

customElements.define('emisor-evento',EmisorEvento)