import {LitElement, html, css} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement {
    
    static get styles() {
        return css `
            :host {
                all: initial;
            }
        `;
    }
    
    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor () {
        super();
        this.showPersonForm = false;
        this.people = [
            {
                profile: "Lorem ipsum dolor sit amet.",
                "name": "Selena Mora",
                "yearsInCompany": 10,
                "photo": {
                    "src": "./img/persona.jpg",
                    "alt": "Selena Mora"
                }
            },
            {
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                "name": "Dolores Fuertes",
                "yearsInCompany": 2,
                "photo": {
                    "src": "./img/persona.jpg",
                    "alt": "Dolores Fuertes"
                }
            },
            {
                profile: "Lorem",
                "name": "Elena Nito del Bosque",
                "yearsInCompany": 5,
                "photo": {
                    "src": "./img/persona.jpg",
                    "alt": "Elena Nito"
                }
            },
            {
                profile: "Lorem ipsum dolor sit amet, consectur...",
                "name": "Leandro Gao",
                "yearsInCompany": 9,
                "photo": {
                    "src": "./img/persona.jpg",
                    "alt": "Leandro Gao"
                }
            },
            {
                profile: "Lorem ipsum dolor sit amet",
                "name": "Angello Marquez",
                "yearsInCompany": 3,
                "photo": {
                    "src": "./img/persona.jpg",
                    "alt": "Angello Marquez"
                }
            }
        ];
    }

    render(){
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <h2 class="text-center">Personas</h2>

            <main>
                <div class="row" id="peopleList">
                    <div class="row row-cols-1 row-cols-sm-4">
                        ${this.people.map(
                            person => html`<persona-ficha-listado name="${person.name}" yearsInCompany="${person.yearsInCompany}" profile="${person.profile}" .photo="${person.photo}" @delete-person="${this.deletePerson}" @info-person="${this.infoPerson}"></persona-ficha-listado>`
                        )}                
                    </div>
                </div>
                <div class="row">
                    <persona-form id="personForm" class="d-none border rounded border-primary" @persona-form-close="${this.personFormClose}" @persona-form-store="${this.personFormStore}"></persona-form>
                </div>
            </main>
        `;
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);
        this.people = this.people.filter(
            person => person.name != e.detail.name // Se armará el arreglo con los datos diferentes al llegado por el evento. Será acumulativo ya que los datos desaparecen de memoria.
        );
    }

    infoPerson(e) {
        console.log("infoPerson en persona-main");
        console.log("Se ha solicitado más información de la persona " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        this.shadowRoot.getElementById("personForm").person = chosenPerson[0];

        this.shadowRoot.getElementById("personForm").editingPerson = true; 

        this.showPersonForm = true;

    }

    updated(changedProperties){
        console.log("updated");
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de showPersonForm en persona-main");
            if (this.showPersonForm === true){
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main");
            this.dispatchEvent(new CustomEvent("update-people",{
                detail: {
                    people: this.people
                }
            })) 
         }
    }

    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando Listado de Personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");	
    } 

    personFormClose() {
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("personFormStore en persona-main");
        console.log(e.detail);	

        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona de nombre " + e.detail.name);

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                ? person = e.detail.person : person);

            let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.name
            );
            if (indexOfPerson >= 0) {
                console.log("Persona encontrada");
                this.people[indexOfPerson] = e.detail;
            }
        } else {
            console.log("Se va a almacenar una persona nueva")
            this.people.push(e.detail);
        }
      
        console.log("Proceso terminado");
        console.log(this.people);
        this.showPersonForm = false;
    }
}

customElements.define('persona-main',PersonaMain)